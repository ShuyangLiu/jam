\documentclass[10pt]{article}

\usepackage{latexsym,comment,amsmath,amsthm,amssymb,graphicx,verbatim,epsfig,url}
% \usepackage{algorithm,algpseudocode,listings}
\usepackage{hyperref}
\usepackage{trfrac}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{syntax}
\usepackage{subcaption}

\newcommand{\sfto}{\,\mathsf{to}\,}
\newcommand{\ttorder}[3]{\mathtt{order(}#1,#2\mathtt{)  \ in\ } #3 }
\newcommand{\sfinit}{\ \mathsf{init}}
\newcommand{\sfhexecuted}{\mathsf{executed}}
\newcommand{\sfhexec}{\mathsf{exec}}
\newcommand{\sfhrf}{\mathsf{rf}}
\newcommand{\sfhvo}{\mathsf{vo}}
\newcommand{\sfhpush}{\mathsf{push}}
\newcommand{\sfhinit}{\mathsf{init}}
\newcommand{\sfhis}{\mathsf{is}}
\newcommand{\sfhwrites}{\mathsf{writes}}
\newcommand{\sfhreads}{\mathsf{reads}}
\newcommand{\sfhexecutable}{\mathsf{executable}}
\newcommand{\sfhedge}{\mathsf{edge}}
\newcommand{\sfhlabel}{\mathsf{label}}
\newcommand{\sfhtagdecl}{\mathsf{tagdecl}}
\newcommand{\sfhacyc}{\mathsf{acyclic}}
% \newcommand{\ttmem}[1]{\mathtt{[}\,#1\,\mathtt{]}_{m}}
% \newcommand{\ttmemm}[2]{\mathtt{[}\,#1\,\mathtt{]}_{#2}}
% \newcommand{\ttmemwr}[2]{\ttmem{#1}\ \mathtt{:=}\ #2}
\newcommand{\ttmem}[1]{#1_{m}}
\newcommand{\ttmemm}[2]{#1_{#2}}
\newcommand{\ttmemwr}[2]{\ttmem{#1}\ \mathtt{:=}\ #2}
\newcommand{\ttmemwrm}[3]{\ttmemm{#1}{#2}\ \mathtt{:=}\ #3}
\newcommand{\ttatmw}[2]{\mathsf{RW}(#1, #2)}
\newcommand{\ttcas}[3]{\mathtt{CAS}(#1, #2, #3)}
\newcommand{\ttfai}[1]{\mathtt{FAI}(#1)}
\newcommand{\histarrow}[1]{\xrightarrow{\mathsf{#1}}_H}
\newcommand{\histarrowH}[2]{\xrightarrow{\mathsf{#1}}_{#2}}
\newcommand{\gsep}{\ |\ }

\renewcommand{\syntleft}{}
\renewcommand{\syntright}{}
\newcommand{\macycporf}{\sfhacyc(\mathsf{po}\ \cup\ \mathsf{rf})}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\begin{document}

\section{Metatheory}

Here, we develop a metatheory for our semantics. We show that using acquire mode
for all reads obviates the inclusion of the JAM's acyclic causality requirement
and we show that properly synchronized programs,
i.e. race-free programs as defined by [cite bohem/adve], will only exhibit
sequentially consistent behaviors as required by Java's "DRF" guarantee.
We also show that each mode, from Volatile to Opaque, admits strictly more
executions (monotonicity). 

These theorems further validate the definitions of our model, give more
evidence that the model is complete with respect to the documentation, and
clearly demonstrate that the semantics is suitable for formal reasoning.

\subsection{Semantics}

\begin{figure}
  \begin{subfigure}[t]{0.35\textwidth}
    \begin{align*}
      \mathsf{Nat} \quad &n := 0 \gsep 1 \gsep \ldots \\ 
      \mathsf{Locations} \quad &l := \ldots \\ 
      \mathsf{Modes} \quad &m := \mathsf{P} \gsep \mathsf{O} \gsep \mathsf{RA} \gsep \mathsf{V} \\ 
      \mathsf{Access\ Ids} \quad & i := ... \\
      \mathsf{Thread\ Ids} \quad & p := ... \\
    \end{align*}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{0.35\textwidth}
    \begin{align*}
      \mathsf{Accesses} \quad & a := \ttmem{l} \gsep \ttmemwr{l}{n} \gsep \ttatmw{l}{n} \\    
      \mathsf{Mem.\ Events}  \quad & h := \sfhinit(i, p) \gsep \sfhis(i, a)  \gsep \sfhexec(i) \\
                                     & \gsep \sfhrf(i, i) \gsep \sfhvo(i, i) \gsep \sfhpush(i, i) \\
      \mathsf{Prog.\ Events}  \quad & d := i = a \gsep i \gsep i\, \sfto\, n \\
      \mathsf{Program} \quad & P := \ldots \\
      \mathsf{History} \quad & H := \epsilon \gsep H,h \\
    \end{align*}
  \end{subfigure}
  \caption{Syntax}
  \label{fig:syntax}
\end{figure}
\begin{figure}
  \begin{subfigure}[t]{0.45\textwidth}
    \vspace{-0.11cm}
    \[
      \trfrac[\ step]{
        P \xrightarrow{d@p} P' \quad
        H \xrightarrow{d@p} H'        
      }{
        (P, H) \rightarrow (P', H')
      }
    \]
    \vspace{0.73cm}
    \[
      \trfrac[\ init]{
        \begin{tralign}
          \neg H(\sfhinit(i, \_)) \quad
        \end{tralign}
      }{
        H \xrightarrow{i = a@p} H, \sfhinit(i, p), \sfhis(i, a)
      }
    \]
  \end{subfigure}
  \hspace{0.25cm}
  \begin{subfigure}[t]{0.50\textwidth}
    \[
      \quad
      \trfrac[\ write]{
        \begin{tralign}
          \mathsf{wf}(H,\sfhexec(i), p) \quad
          % &H(\sfhinit(i, p)) \\ 
          % &H(\sfhis(i, \ttmemwr{l}{n})) \\
          % &\neg H(\sfhexec(i)) \\ %% executable(i)
          \sfhacyc(\histarrowH{co}{H,\sfhexec(i)})
        \end{tralign}
      }{
        H \xrightarrow{i@p} H, \sfhexec(i)
      }
    \]
    \[
      \trfrac[\ read]{
        \begin{trgather}
          \mathsf{wf}(H, \sfhrf(i_w, i), p, n) \quad 
          \begin{tralign}
            \sfhacyc(\histarrowH{co}{H,\sfhrf(i_w, i)}) \\
            \mathsf{acyclic}(\histarrowH{\mathsf{po}\, \cup\, \mathsf{rf}}{H,\sfhrf(i_w, i)})            
          \end{tralign} 
          % H(\sfhexec(iw)) \lor (\exists\ iw',\ H(\sfhrf(iw', iw)) \land     H(\sfhis(iw, \ttatmw{l}{n}))) \\
          % \begin{tralign}
          %   & H(\sfhinit(i, p)) \\
          %   & H(\sfhis(i, a))\\ 
          %   & a \in \{\ttmem{l}, \ttatmw{l}{n}\}
          % \end{tralign} \quad 
          % \begin{tralign}
          %   & \neg H(\sfhrf(\_,i)) \\ 
          %   & \sfhacyc(\histarrowH{co}{H,\sfhrf(iw, i)}) \\
          %   & \sfhwrites(H, iw, l, n)
          % \end{tralign} 
        \end{trgather}
      }{
        H \xrightarrow{i \sfto n@p} H, \sfhrf(i_w, i)
      }
    \]
  \end{subfigure}
  
  \begin{align*}
    \sfhreads(H, i, l)  & \triangleq \exists\, m, H(\sfhis(i, \ttmem{l})) \lor
                          \exists\, n, H(\sfhis(i, \ttatmw{l}{n})) \\ 
    \sfhwrites(H, i, l, n) & \triangleq \exists\, m, H(\sfhis(i, \ttmemwr{l}{n})) \lor H(\sfhis(i, \ttatmw{l}{n})) \\
    \sfhexecuted(H, i) & \triangleq H(\sfhexec(i)) \lor \exists\, i_w, H(\sfhrf(i_w, i)) \\
    \sfhexecutable(H, i) & \triangleq \neg\, \sfhexecuted(H, i) \land \forall i', i' \histarrowH{\mathsf{into}}{H} i \xRightarrow{\quad} \sfhexecuted(H, i')
  \end{align*}
  \[
    \begin{trgather}
      % H(h) \triangleq h \in H \\
      % \\
      \mathsf{wf}(H,\sfhexec(i), p) \triangleq 
      \begin{cases}
        & H(\sfhinit(i, p)) \\ 
        & H(\sfhis(i, \ttmemwr{l}{\_})) \\
        & \sfhexecutable(H, i) % \\ %% executable(i)
        % (l:L):i=a & \mathsf{if}\ d = (L:i=a) \\
        % d & \mathsf{otherwise}
      \end{cases}
    \end{trgather}
    \quad
    \mathsf{wf}(H, \sfhrf(i_w, i), p, n) \triangleq 
    \begin{cases}
      & H(\sfhinit(i, p)) \\
      & \sfhreads(H, i, l) \\
      & \sfhwrites(H, i_w, l, n)\\ % H(\sfhis(i, \ttmemwr{l}{\_})) \\
      & \sfhexecuted(H, i_w) \\
      & \sfhexecutable(H, i) % \\ %% executable(i)
    \end{cases}
  \]
  \caption{Semantics}
  \label{fig:semantics}
\end{figure}

We have ported our semantics to Coq by adapting the history fragment of the
semantics of Crary and Sulivan [cite popl15].

The syntax of our formalization appears in Figure \ref{fig:syntax}. We use
$n$, $l$, $i$, and $p$ to range over natural numbers, memory locations, unique
memory access identifiers, and unique thread identifiers. We use $m$ to
represent one of the four access modes, $\mathsf{P}$ for plain, $\mathsf{O}$ for
opaque, $\mathsf{RA}$ for release acquire and $\mathsf{V}$ for volatile. Note
that $\mathsf{RA}$ writes are release writes and $\mathsf{RA}$ reads are acquire
reads.

Memory accesses, $a$, can take the form of reads, $\ttmem{l}$, writes
$\ttmemwr{l}{n}$ with their accompanying modes and read-writes $\ttatmw{l}{n}$.

Memory events, $h$ record the program's interactions with memory. Notably, we
assume that the program and history can record specified visibility and
specified push orders, $\sfhvo(i, i)$ and $\sfhpush(i, i)$. For example, the
program may use the labeling and ordering mechanism of Crary and Sulivan [cite].
We discuss the other events in detail below.

We use $d$ to range over program generated events which are translated by memory
semantics into memory events and we use $P$ to abstract over an expression
language that can produce such events. We use $H$ to represent a list
of memory events, where $H(h)$ means that $h \in H$.

Finally we use $i \histarrow{R} i$ to represent memory model relations for $H$.
In what follows the restriction on $\mathsf{to}$ and the acyclicity requirements
form the interface with the relations of our axiomatic model. We give a full and
direct mapping from the definitions of the herd model to the Coq definitions of
the corresponding relations in appendix ??.

Program and history states transition together via the step relation defined in
figure \ref{fig:semantics}. The history transition semantics certifies program
events $d@p$ of the form $i = a@p$, $i @ p$, or $i \sfto n @ p$ and turns them
into memory events.

$i = a @ p$ represents the ``initialization'' of a memory access $a$ using the
unique identifier $i$ in thread $p$. The history semantics appends $\sfhinit(i,
p)$ and $\sfhis(i, a)$ to $H$ to record the initialization and the form of the
memory access identified by $i$. The only certification required of an
initialization is that the memory access identified by $i$ is not already
initialized. Importantly, we assume that the program initializes memory accesses
in program order, so the subsequence of inititalization events records program
order in $H$.

$i @ p$ represents the execution of a write in thread $p$ which is recorded in
history with $\sfhexec(i)$. Writes must be certified by the acyclicity
requirement for $\mathsf{co}$ and a basic well-formedness condition
$\mathsf{wf}(H, \sfhexec(i), p)$. The well-formedness requires that $i$ be
initialized $H(\sfhinit(i, p))$, that the memory access associated with $i$ be a
write, $H(\sfhis(i, \ttmemwr{l}{n}))$, and that the write be executable
$\sfhexecutable(H, i)$.

The $\sfhexecutable(H, i)$ constraint requires that $i$ has not already executed
and that the sequence of execution, $\mathsf{to}$, respects any intra-thread
ordering, $\mathsf{into}$, as in the description of section ?? (fences
subsection of semantics). The program is otherwise free to execute memory
accesses out-of-order.

$i \sfto n @ p$ represents the execution of a read $i$, reading the value $n$ in
thread $p$ which is recorded in history with $\sfhrf(i_w, i)$. Reads must be
certified by the same acyclicity requirement for $\mathsf{co}$ and the
additional requirement on $\mathsf{po} \cup \mathsf{rf}$. The well-formedness
condition for reads requires that $i$ be initialized, that it be a read or a
read-write, $\sfhreads(H, i, l)$, and that it be executable. It further requires
that the paired write $i_w$ is executed $\sfhexecuted(H, i_w)$ and that $i_w$
writes the value $n$ to the same location $l$, $\sfhwrites(H, i, l, n)$.

\subsection{Theorems}

We will first establish that the visibility order of our model is irreflexive.
We will then give our three main theorems and sketch the key idea of the proofs
for each. For a fully formal treatment we refer the interested reader to the
supplementary material which contains the semantics, lemmas and theorems which
have been mechanized in Coq.

TODO reference which files for each lemma and theorem

To start, we unify the well-formedness conditions of the history semantics as a
single concept, trace coherence. We assume trace coherence for all of our lemmas
and theorems to satisfy basic expectations about the well-formedness of
histories. (TODO reference definition of coherence for cross check)

\begin{lemma}[Irreflexive Visibility]\label{lem:irrvo}
  Suppose $H$ is trace coherent then, for all accesses $i$, $\neg\, i \histarrow{\mathsf{vo}+} i$.
\end{lemma}

\begin{proof}
  This follows from two facts. First, visibility derives from orderings that are
  always either program ordered from intra-thread synchronization (\textsf{svo},
  \textsf{spush}, \textsf{ra}, \textsf{volint}) or trace ordered
  (\textsf{pushto}, \textsf{rf}). Second, both relations are total and
  \textsf{to} is consistent with the \textsf{po} edges for intra-thread
  visibility by $\sfhexecutable$.
\end{proof}

Next we show that if all reads are acquire-reads then the JAM's acyclic
causality requirement is unnecessary. This theorem demonstrates the soundness of
proposed compiler implementations for satisfying the acyclic causality
requirement [cite demsky] of the JAM. Note that our assumptions are slightly
stronger than necessary since not all reads need to be acquire-reads to forbid
causal cycles, only those preceding writes in program order.

\begin{theorem}[Causal Acquire-Reads]
  Suppose that $H$ is trace coherent, all reads in $H$ are acquire-reads, and
  all accesses are opaque or stronger, then $\sfhacyc(\histarrow{\mathsf{po} \cup \mathsf{rf}})$.
\end{theorem}

\begin{proof}

We assume $i \histarrow{\mathsf{po} \cup \mathsf{rf}+} i$ for some $i$ and show a contradiction.
By Lemma \ref{lem:irrvo} is enough to demonstrate a cycle in $\mathsf{vo}+$.

First, note that any sequence
$i_1 \histarrow{\mathsf{rf}} i_2 \histarrow{\mathsf{po}} i_3$ implies $i_1 \histarrow{\mathsf{vo}+} i_3$
because \textsf{rf} implies \textsf{vo} and
because, by assumption, the read $i_2$ is an acquire read and
$i_3$ is program order later and must be opaque so $i_2 \histarrow{\mathsf{vo}} i_3$.

Let $\histarrow{\mathsf{rfpoq}}$ be a reads-from edge followed by an optional
program order edge. Note that, by induction and the fact
that \textsf{rf} and \textsf{rfpo} imply \textsf{vo} we can show that $i_1
\histarrow{\mathsf{rfpoq}} i_2$ implies $i_1 \histarrow{\mathsf{vo}+} i_2$

Then, since $\mathsf{po}$ is irreflexive, we have that any
sequence $i \histarrow{\mathsf{po} \cup \mathsf{rf}+} i$ must include at least
one $\mathsf{rf}$ edge. Thus we can rearrange to get 
$i' \histarrow{\mathsf{rfpoq}+} i'$ and we have $i' \histarrow{\mathsf{vo}+} i'$
by the above, as required.
\end{proof}

Next we show that if a program is properly synchronized then it will only
exhibit sequentially consistent behavior. We require the following standard definitions
including the traditional notion of sequential consistency [cite shasha snir]:
\begin{align*}
  i_1 \histarrow{\mathsf{fr}} i_2 &\triangleq \exists\, i_3, i_3
    \histarrow{\mathsf{rf}} i_1 \land i_3 \histarrow{\mathsf{co}} i_2 \\
  i_1 \histarrow{\mathsf{com}} i_2 &\triangleq i_1 \histarrow{\mathsf{co}} i_2 \lor i_1 \histarrow{\mathsf{rf}} i_2 \lor i_1 \histarrow{\mathsf{fr}} i_2 \\
  i_1 \histarrow{\mathsf{sc}} i_2 &\triangleq i_1 \histarrow{\mathsf{po}} i_2 \lor i_1 \histarrow{\mathsf{com}} i_2 \\
\end{align*}

We also require a definition of proper synchronization in keeping with our
focus on visibility.
\begin{align*}
  i_1 \histarrow{\mathsf{sync}} i_2 &\triangleq \exists i_3, i_1 \histarrow{\mathsf{vo}+} i_3 \histarrow{\mathsf{po}*} i_2 \land \forall i_4, i_3\histarrow{\mathsf{po}*} i_4 \xRightarrow{} i_3 \histarrow{\mathsf{vo}} i_4
\end{align*}

The idea is that any conflicting access is ordered by some visibility mechanism,
be it a specified order (fence) or a strong access mode access for $i_3$. Then,
following the definition of ``type 2'' data-races from [cite bohem], we say that
$H$ is \textit{race-free} when, for all conflicting accesses $i_1$
and $i_2$, we have $i_1 \histarrow{\mathsf{sync}} i_2$ or $i_2 \histarrow{\mathsf{sync}} i_1$.

\begin{theorem}[DRF-SC]
  Suppose that $H$ is trace coherent and race free. Further suppose
  that $\sfhacyc(\histarrow{\mathsf{co}})$, then $\sfhacyc(\histarrow{\mathsf{sc}})$.
\end{theorem}

\begin{proof}
We assume $i \histarrow{\mathsf{sc+}} i$ for some $i$ and show a contradiction.
By Lemma \ref{lem:irrvo} is enough to demonstrate a cycle in $\mathsf{vo}+$.

We can show that for any $i_1$ and $i_2$,
if we have
$i_1 \histarrow{\mathsf{com}} i_2$
then we have
$i_1 \histarrow{\mathsf{sync}} i_2$.
Note that any accesses related by \textsf{com}
are conflicting. Then we have either 
$i_1 \histarrow{\mathsf{sync}} i_2$ or $i_2 \histarrow{\mathsf{sync}} i_1$.
In each case for \textsf{com} we can show that
$i_2 \histarrow{\mathsf{sync}} i_1$ creates a cycle in the coherence order so
it must be $i_1 \histarrow{\mathsf{sync}} i_2$.

Then, since $\mathsf{po}$ is irreflexive, we have that any
sequence $i \histarrow{\mathsf{sc}+} i$ must include at least
one $\mathsf{com}$ edge.
Then since \textsf{com} edges are also \textsf{sync}
edges, when we have 
$i_1 \histarrow{\mathsf{sc}+} i_2$
we also have 
$i_1 \histarrow{\mathsf{po} \cup \mathsf{com}+} i_2$
and therefore
$i_1 \histarrow{\mathsf{po} \cup \mathsf{sync}+} i_2$.

But then we can rearrange a cycle in
$i \histarrow{\mathsf{po} \cup \mathsf{sync}+} i$
to be
$i \histarrow{\mathsf{sync}+} i$
by appending a leading \textsf{po} edge to the end.
Observe that for any sequence
$i_1 \histarrow{\mathsf{sync}+} i_2$
we have
$i_1 \histarrow{\mathsf{vo}+} i_2$,
so we have 
$i \histarrow{\mathsf{vo}+} i$
as required.
\end{proof}

Finally, we demonstrate the monotonicity of our access mode definitions following
the example of [c11popl15]. We define the reflexive ordering of the access modes as $\mathsf{P}
\sqsubseteq \mathsf{O} \sqsubseteq \mathsf{RA} \sqsubseteq \mathsf{V}$ and
extend it to accesses
$\ttmemm{l}{m_1} \sqsubseteq \ttmemm{l}{m_2}$,
$\ttmemwrm{l}{m_1}{n_1} \sqsubseteq \ttmemwrm{l}{m_2}{n_2}$, 
$\ttatmw{l}{n_1} \sqsubseteq \ttatmw{l}{n_2}$ whenever $m_1 \sqsubseteq m_2$.
As a technical matter we treat read-writes as always having the same order.
We extend the order to histories by matching identifiers and ordering the
accesses. 
\[
  H_1 \sqsubseteq H_2 \triangleq \forall\, i\, a_1\, a_2, H_1(\sfhis(i, a_1)) \land H_2(\sfhis(i, a_2))
  \xRightarrow{} a_1 \sqsubseteq a_2 
\]

When the \textsf{po}, \textsf{rf}, and \textsf{to} relations of two histories
$H_1$ and $H_2$ have the following relationships: $\histarrowH{po}{H_2} \subseteq \histarrowH{po}{H_1}$ , $\histarrowH{to}{H_2}
\subseteq \histarrowH{to}{H_1}$, $\histarrowH{rf}{H_2} \subseteq
\histarrowH{rf}{H_1}$, then we say they \textit{match}.

\begin{theorem}[Monotonicity]
  For two histories $H_1$ and $H_2$, suppose that both match, both are trace
  coherent, and $H_2 \sqsubseteq H_1$. Further suppose that
  $\sfhacyc(\histarrowH{co}{H_1})$ and that there are no specified visibility
  orders or push orders in $H_2$, then $\sfhacyc(\histarrowH{co}{H_2})$
\end{theorem}

We make two notes. First the absence of specified orders in $H_2$ is a
technical convenience since specified order edges are not related
to the strength of the access modes for reads and writes.
Second, we focus on the acyclic coherence requirement because the match assumption
means that it would be trivial to satisfy the acyclic causality requirement for
$H_2$ supposing it is true of $H_1$ because \textsf{po} and \textsf{rf} have
fewer edges in $H_2$.

\begin{proof}
  We assume
  $i \histarrowH{\mathsf{co}}{H_2} i$
  for some $i$ and show that this must mean
  $i \histarrowH{\mathsf{co}}{H_1} i$ which is a contradiction.
  This is straight forward by induction on
  $i \histarrowH{\mathsf{co}}{H_2} i$, noting that each case of visibility
  in $H_2$ will exist in $H_1$ because of stronger access modes in $H_1$.
\end{proof}

\end{document}