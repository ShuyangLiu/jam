readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly MODEL="rc11"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

echo "begin repairing-sc"
for test in $(ls $LITMUS_DIR/repairing-sc/); do
  herd_clean rc11 "repairing-sc/$test"
  herd_clean jam "repairing-sc/$test"
done

echo "begin taming"
for test in $(ls $LITMUS_DIR/taming/); do
  herd_clean rc11 "taming/$test"
  herd_clean jam "taming/$test"
done

echo "begin opt invalid"
for test in $(ls $LITMUS_DIR/opt-invalid/); do
  herd_clean rc11 "opt-invalid/$test"
  herd_clean jam "opt-invalid/$test"
done

echo "begin own"
# borrowed from ARMv8 to verify claim's about C for this test
herd_clean rc11 "IRIW-sc-rlx-acq.litmus"
herd_clean jam "IRIW-sc-rlx-acq.litmus"

