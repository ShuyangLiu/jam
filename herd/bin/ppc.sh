readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly MODEL="ppc"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

herd_simple $MODEL "hotspot-sc.litmus"
exit 0;
