* TODO questions
  - compatibility with JSR133
  - what would doug like to see in the model?
  - volatile is not SC accesses
  - should volatile accesses have intra-thread ordering with non-volatile?
    - example fence implimentation (fence after writes/reads) suggests yes
    - optimized version from Batty (all SC writes have fence) suggests yes, but only for writes
    - the definition where SC accesses are release writes or acquire reads and also 
      order with later non-sc accesses means that SC acccesses act like full funces
  - should volatile accesses have cross-thread ordering with non-volatile?
    - almost certainly not based on the documentation page from doug
  - should atomic rws have intra-thread ordering across locations?
    - no?
    - at the end of the RMC paper they suggest that atomic accesse do have 
      ordering. This is true on x86 lock xchg but is it true elsewhere?
  - what is the purpose of an atomic read write if there is already a per-location total order 
    on writes in C++?
    - answer: atomic reads ordered with writes
  - what is the purpose of SC accesses when we have atomic read-writes?
    - atomic rws do not have intra/inter-thread ordering across locations
  - are the SC fences of Batty et al. like our push orders?
  - non-atomic reads and writes in the racey definition
    - is the reason they have the non-atomic requirement in the definition of 
      racy that the drf concerns/guarantees are only for non-atomic accesses
    - do we need non-atomic acccesses in our semantics for those litmus tests
      to make sense? (yes)
  - modeling plain accesses where hb is only rf
* issues for a paper
** novelty
   - how is this different from Batty's model of C11?
     - atomic read writes without total order on writes
     - no total order on writes until vol/atomic rws
     - acyclic(po U rf)
     - orders
       - allows reasoning in mode weaker than opaque with thin-air reads
     - could model plain mode
     - operational
     - promising semantics paper claims SC fences between all accesses
       doesn't guarantee SC semantics 
       #+begin_quote
       Our model lacks SC accesses, and its condition for SC fences is stronger
       than the one of C++11. In particular, unlike in C++11, placing an SC
       fence between every two commands does guarantee SC semantics.
       #+end_quote
       - should be a theorem, vol => sc
     - lock/unlock?
** contribution
   - java memory model
   - theorems
   - litmus tests
* evaluation
** litmus tests
*** proof
    - requires being able to easily show acyclic(co) and acycli(po U rf)
      - see SeqCon.v for topo sort lemma
    - can operate on histories only, assuming constructed po
    - forbidden oota 
    - split by modes
**** Opaque 
     - many/all hardware litmus tests
**** Release/Acquire
     - forbids oota examples without predicate
**** Volatile
     - dekker 
       - used in Doug's post
     - iriw
       - used in Doug's post
**** atomic rws
     - read chains totally ordered
*** run programs
    - implement svo, push 
    - run programs, observe behaviors
    - would be interesting to see if there are existing discrepancies 
    - any difference between volatiles and sc atomics?
** theorems
*** total order for atomic rws
    - single 
    - chains
*** semantics --> trco 
*** vol access --> sc semantics
    - total order/intra thread ordering are by definition
    - reads from by co-wr
*** push orders --> sc semantics 
    - push order vs push changes specified SC (ssc) slightly
*** existing DRF
    - for opaque mode, should hold
*** preserved optimizations
    - strengthening (promising)
      - subset of histories: H_o' <= H_o if o <= o'
      - not that interesting given construction by layered rules
    - merging 
      - see On validity of program transformations in the Java Memory Model
      - read after write is noted in the promising semantics paper
*** JOM relationship with original JMM
*** atomic accesses + (po --> vo) --> sc semantics
    - see question to doug about intra-thread cross location ordering
* notes for the paper
  - race free definition should come before racefree definition in herd model 
    for the purposes of comparing with RC11 using the opt-invalid reordering tests
  - rf as a part of vo means hb is larger than the hb of C11 
    - defining races requires removing it
  - sc accesses have a strict total order derived from trace order, as outlined
    in the RC11 paper
  - split rmw's are accomodated in the litmus tests by using a reflexive relation 
    over singel instruction rmws 
  - add quote to end of section 2 about accumulating guarantees
  - need thin-air read litmus tesots
  - need a noted in a readme for the artifact about how we use SC insteaf of VOL etc
    from the paper
* proofs
** TODO definition of sb in DRF
   - should assume opaque mode accesses 
   - make a note in the paper that we plain mode gets no such guarantees 
     
* paper
** TODO define fences in terms of specified orders 
** TODO monotonicity proof
   - how does it go
** TODO are our meta theorems different (check RC11)     
   - vo+ cycle 
   - co cycles (sc => po sb)
** TODO outline
*** TODO paper
*** TODO abstract
*** TODO introduction
    - related work
      - find quote where C11 is different (wait on jens)

* total order
  - with final value co specified
    - comparison against aarch64
      - really just depends on co being observable
      - alternately:
        
        even if we made co observable in the way they have defined it for 
        armv8 we would never have an edge between the two writes

    - comparison against self 
      - atomic rw example 
        - one where the total order is implemented
          - both cases give a different outcome from when it is not implemented
        - one where it's not
  - wo final value co specified
    - comparison against aarch64
      - really just depends on co being observable
    - comparison against self 


* prepub
** paper   
   - DONE keywords
   - DONE first reference of thin-air read in section 3 
   - DONE mapping from specified orders to fences
   - DONE appendix with example program and graphs for totalco
   - DONE conclusion
   - DONE appendix for full model
   - DONE citations/references/bib
     - DONE Formalizing java's drf
   - DONE 23 pages
     - figure formatting 
   - DONE ask jens about the paper he referenced in his feedback on related work.
** supplementary material
   - readme describing what's included
     - DONE links to the litmus tests 
     - DONE links to other models
     - DONE include custom ones 
     - DONE remove tests not in the paper
   - coq subdirectory 
     - TODO mapping from herd model
       - note discrepancies
     - DONE rename HB to vo 
     - DONE rename vo to volvo
     - rename anything else that arises from herd mapping 
   - herd subdirectory 
     - DONE single model 
       - DONE appendix too
     - DONE include herd subdirectory 
