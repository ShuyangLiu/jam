graph_index=0
file=$1
file_prefix=$(echo $file | sed 's/\.dot$//');
new_file="$file_prefix$graph_index.dot"
while IFS= read -r line; do
  if echo $line | grep digraph > /dev/null; then
    new_file="$file_prefix$graph_index"
    echo "digraph G$graph_index {" > "$new_file.dot"
    echo "$new_file"
    ((graph_index++))
  else
    echo "$line" >> "$new_file.dot"
  fi
done < $file
