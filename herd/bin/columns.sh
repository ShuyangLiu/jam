echo "| test | aarch64 | jam |"
while read line; do
  model=$(echo $line | sed 's/HERD: model //' | sed 's/ for test.*//');
  test=$(echo $line | sed 's/.*for test \(.*\) with .*/\1/');
  result=$(echo $line | sed 's/.*\(Never\|Sometimes\|Always\|TIMEOUT\/OOM\).*/\1/');

  if ! [ "$test" == "$line" ] && ! [ "$new_row" == "true" ]; then
    echo -n "| $test | ";
  fi

  if ! [ "$result" == "$line" ]; then
    echo -n "$result | "
    if [ "$new_row" == "true" ]; then
      echo
      new_row=false
    else
      new_row=true
    fi
  fi
done
echo
