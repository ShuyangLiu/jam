apt-get update
apt-get install -y ocaml-nox opam m4

pushd /tmp
rm -rf master.zip
wget -q https://github.com/pshved/timeout/archive/master.zip
unzip -o master.zip
cp -r timeout-master/timeout /usr/local/bin/
chmod +x /usr/local/bin/timeout
popd
