
output_dir="output"
name="$1"
file="$1.tex"

build(){
  echo $1
  latex --shell-escape --file-line-error --output-format=pdf $1
}

build_bib (){
  build "$2"
  bibtex "$1"
  build "$2"
  build "$2"
}

pushd "$2"

# build once by default
build_bib $name "$file"

cat "$file" > "/tmp/$file"

# watch for alterations
while true; do
  # if there's a difference
  if ! diff "/tmp/$file" "$file" > /dev/null; then
    build_bib $name "$file"
    cat "$file" > "/tmp/$file"
  else
    sleep 1
  fi
done

popd -n
