"Java Volatile Mode"

let opq = O | RA | SC
let rel = W & RA
let acq = R & RA
let vol = V

(* release acquire ordering *)
let ra = po;[rel] | [acq];po

(* intra thread volatile ordering *)
let volint =  po;[vol & R] | [vol & W];po

(* intrathread ordering contraints *)
let into = svo | spush | ra | volint

(* define trace order, ensure it respects rf and intra thread specified orders *)
(* NOTE ((W * FW) & loc & ~id) = cofw *)
with to from linearisations(M\IW, ((W * FW) & loc & ~id) | rf | into)

(* cross thread push ordering extended with volatile memory accesses *)
let push = spush | volint
let pushto = to+ & (domain(push) * domain(push))

(* extend ra visibility *)
let vvo = rf | svo | ra | push | pushto;push
let vo = vvo+ | po-loc

include "filters.cat"

let WWco(rel) = WW(rel) & loc & ~id

(* initial writes are before everything *)
let coinit = loc & IW*(W\IW)

(* final writes are co-after everything *)
let cofw = WWco((W * FW))

(* jom coherence *)
let coww = WWco(vo)
let cowr = WWco(vo;invrf)
let corw = WWco(vo;po)
let corr = WWco(rf;po;invrf)

(* borrow general definition from RC11 that will work for atomic rws and split instruction rws *)
let rmw-jom = [RMW] | rmw

(* use to define when they are executed *)
let cormwtotal = WWco(((range(rmw-jom) * _ ) | (_ * range(rmw-jom))) & to)

(* i1 -rf> ir -rmw> i2 /\ i1 -co> i3 => i2 -co> i3 atm coherence *)
let cormwexcl = WWco((rf;rmw-jom)^-1;co-jom)

let rec co-jom = coww
  | cowr
  | corw
  | corr
  | cormwtotal
  | WWco((rf;rmw-jom)^-1;co-jom)
  | coinit
  | cofw

acyclic (po | rf) & opq
acyclic co-jom as co-cycle
show co-jom, cofw
