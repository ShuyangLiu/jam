# NOTE this will require 4 gigs of ram :(
export OPAMROOT=~/.opam # installation directory
opam init -n --comp=4.02.3
opam repo add coq-released http://coq.inria.fr/opam/released
opam install coq.8.6.1
opam pin add coq 8.6.1
opam install herdtools7
echo "eval `opam config env`" > ~/.bash_profile

mkdir -p ~/jam
cp -r /vagrant/* ~/jam

